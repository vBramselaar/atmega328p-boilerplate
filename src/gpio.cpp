#include "gpio.h"

uint8_t Gpio::digitalRead(int pin)
{
 	if (pin > 7)
	{
 		pin -= 8;
 		return !(PINB & (1u << pin));
 	}
 	else if (pin <= 7)
	{
 		return !(PIND & (1u << pin));
 	}

	return 0;
}

void Gpio::digitalWrite(int pin, bool mode)
{
	if (pin > 7)
	{
		pin -= 8;
		if (mode)
		{
			PORTB |= (1u << pin);
		}
		else
		{
			PORTB &= ~(1u << pin);
		}
	}
	else if (pin <= 7)
	{
		if (mode)
		{
			PORTD |= (1u << pin);
		}
		else
		{
			PORTD &= ~(1u << pin);
		}
	}
}

void Gpio::pinMode(int pin, bool mode)
{
	if (pin > 7)
	{
		pin -= 8;
		if (mode)
		{
			DDRB |= (1u << pin);
		}
		else
		{
			DDRB &= ~(1u << pin);
		}
	}
	else if (pin <= 7)
	{
		if (mode)
		{
			DDRD |= (1u << pin);
		}
		else
		{
			DDRD &= ~(1u << pin);
		}
	}
} 
