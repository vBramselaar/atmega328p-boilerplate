#ifndef BVD_GPIO_H
#define BVD_GPIO_H

#include <avr/io.h>

class Gpio
{
public:
	static void pinMode(int pin, bool mode);
	static void digitalWrite(int pin, bool mode);
	static uint8_t digitalRead(int pin);
};

#endif
