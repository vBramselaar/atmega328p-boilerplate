#include <avr/io.h>
#include "gpio.h"

int main(void)
{
	Gpio::pinMode(13, true);
	bool led = true;
	
	while(true)
	{
		for (volatile long i = 0; i < 100000; i++);

		Gpio::digitalWrite(13, led);
		led = !led;
	}

	return 0;
}
